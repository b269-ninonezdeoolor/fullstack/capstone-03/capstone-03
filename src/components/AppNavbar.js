import { useContext, useEffect, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import UserContext from '../UserContext';

export default function AppNavbar() {
  const { user } = useContext(UserContext);
  const [token, setToken] = useState(null);
  const [userDetails, setUserDetails] = useState(null); // new state variable
  const [isAdmin, setIsAdmin] = useState(false);

  useEffect(() => {
    const storedToken = localStorage.getItem('token');
    setToken(storedToken);
  }, [token, user]);

  useEffect(() => {
    if (token) {
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setUserDetails(data);
          setIsAdmin(data.isAdmin);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }, [token]);

  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand as={Link} to="#">
        E COMMERCE
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="me-auto">
          {token !== null && user !== null && (
            <>
              {isAdmin === false ? (
                <>
                  <Nav.Link as={NavLink} to="/products">
                    Products
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/cart">
                    Cart
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/logout">
                    Logout
                  </Nav.Link>
                </>
              ) : (
                <>
                  <Nav.Link as={NavLink} to="/ManageProducts">
                    Products
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/ManageUsers">
                    Users
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/logout">
                    Logout
                  </Nav.Link>
                </>
              )}
            </>
          )}
          {token === null && (
            <>
              <Nav.Link as={NavLink} to="/">
                Login
              </Nav.Link>
              <Nav.Link as={NavLink} to="/register">
                Register
              </Nav.Link>
            </>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}