import AppNavbar                                  from './components/AppNavbar';
import Error                                      from './components/Error';
import Register                                   from "./pages/Register";
import Login                                      from "./pages/Login";
import Logout                                     from './pages/Logout';
import ManageProducts                             from "./admin/ManageProducts";
import ManageUsers                                from "./admin/ManageUsers";
import Products                                   from "./user/Products";
import Cart                                       from "./user/Cart";
import { Container }                              from 'react-bootstrap';
import { UserProvider }                           from './UserContext';
import { useState, useEffect }                    from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.css';


export default function App() 
{
  const [user, setUser] = useState({
    id: null, 
    isAdmin: null
  });

  const unsetUser = () => 
  {
    localStorage.clear();
  }

  useEffect(() => 
  {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, 
    {
      headers: 
      {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => 
      {
        // User is logged in
        if(typeof data.id !== 'undefined') 
        {
          setUser({
            id: data.id, 
            isAdmin: data.isAdmin
          })
        }
        // User is logged out
        else 
        {
          setUser({
            id: null, 
            isAdmin: null
          })
        }
      })
    .catch(error => console.error(error));
  }, []);

  return (
    // <></> - common pattern in React for component to return multiple elements
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <Container>
          <AppNavbar/>
            <Routes>
              <Route path="/"                 element={<Login />} />
              <Route path="/logout"           element={<Logout />} />
              <Route path="/register"         element={<Register />} />
              <Route path="/ManageProducts"   element={<ManageProducts />} />
              <Route path="/ManageUsers"      element={<ManageUsers />} />
              <Route path="/products"         element={<Products />} />
              <Route path="/cart"             element={<Cart />} />
              <Route path="/*"                element={<Error />} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}