import { useState, useEffect } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Table from 'react-bootstrap/Table';
import Pagination from 'react-bootstrap/Pagination';
import Swal from 'sweetalert2';


export default function ManageProducts()
{
  const token = localStorage.getItem('token');
  const [userDetails, setUserDetails] = useState({});
  const [products, setProducts] = useState([]);
  const [productCount, setProductCount] = useState(0);
  const [searchTerm, setSearchTerm] = useState('');
  const [viewModalShow, setViewModalShow] = useState(false);
  const [updateModalShow, setUpdateModalShow] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState({});
  const [currentPage, setCurrentPage] = useState(0);
  const [pageCount, setPageCount] = useState(0);
  const productsPerPage = 10;
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);


  useEffect(() => {
    if (token) {
      fetch(`${process.env.REACT_APP_API_URL}/products/viewAllProducts`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          // sort products array in descending order based on createdOn field
          const sortedProducts = data.sort((a, b) => new Date(b.createdOn) - new Date(a.createdOn));
          setProducts(sortedProducts);
          setPageCount(Math.ceil(sortedProducts.length / productsPerPage));
        })
        .catch((error) => {
          console.log(error);
        });
       fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setUserDetails(data);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }, [token]);


  const handleViewProduct = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/viewAproduct`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    .then(response => response.json())
    .then(data => {
      if (data && data.product) {
        console.log(data); 
        setSelectedProduct(data.product);
        setViewModalShow(true);
      } else {
        console.error("Invalid data returned from API");
      }
    })
    .catch(error => console.error(error));
  };
  

  const handleUpdateProduct = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/viewAproduct`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        if (data && data.product) {
          console.log(data);
          setSelectedProduct(data.product);
          setUpdateModalShow(true);
        } else {
          console.error("Invalid data returned from API");
        }
      })
      .catch((error) => console.error(error));
  };


  const handleSaveChanges = () => {
    const productName = document.querySelector("#formProductName").value;
    const productDescription = document.querySelector("#formProductDescription").value;
    const productStocks = document.querySelector("#formProductStocks").value;
    const productPrice = document.querySelector("#formProductPrice").value;
     if (!productName || !productDescription || !productStocks || !productPrice) {
      console.error("");
      Swal.fire({
        title: 'FAILED!',
        icon: 'error',
        text: 'Empty field is not allowed.',
      });
      return;
    }
     fetch(`${process.env.REACT_APP_API_URL}/products/${selectedProduct._id}/updateProduct`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        productName: productName,
        productDescription: productDescription,
        productStocks: productStocks,
        productPrice: productPrice,
      }),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error(`HTTP error! status: ${response.status}`);
        }
        return response.json();
      })
      .then((data) => {
        if (data && data.success) {
          console.log("Product updated successfully");
          Swal.fire({
            title: 'SUCCESS!',
            icon: 'success',
            text: 'Product updated successfully.',
          });
          setUpdateModalShow(false);
          
        } else if (data && !data.success) {
          console.error("Failed to update the product");
        } else {
          console.error("No data found in response");
        }
      })
      .catch((error) => console.error(error));
  };


// Function to handle archiving a product
const archiveProduct = async (productId) => {
  try {
    const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
      method: 'PATCH',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    });
    const data = await response.json();
    // update state or do something else with the response data
    // reload the page
    Swal.fire({
      title: 'SUCCESS!',
      icon: 'success',
      text: 'Archived successfully.',
    });
    setTimeout(() => {
      window.location.reload();
    }, 2000);
  } catch (error) {
    console.error(error);
  }
};


 // Function to handle unarchiving a product
const unarchiveProduct = async (productId) => {
  try {
    const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/unArchive`, {
      method: 'PATCH',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    });
    const data = await response.json();
    // update state or do something else with the response data
    // reload the page
    Swal.fire({
      title: 'SUCCESS!',
      icon: 'success',
      text: 'Unarchived successfully.',
    });
    setTimeout(() => {
      window.location.reload();
    }, 2000);
  } catch (error) {
    console.error(error);
  }
};


  const handlePageChange = (newPage) => {
    if (newPage >= 0 && newPage < pageCount) {
      setCurrentPage(newPage);
    }
  };

  const filteredProducts = products.filter((product) =>
    product.productName.toLowerCase().includes(searchTerm.toLowerCase()) || 
    product.productDescription.toLowerCase().includes(searchTerm.toLowerCase()) ||
    product.productCode.toLowerCase().includes(searchTerm.toLowerCase())
  )


  const displayedProducts = filteredProducts.slice(
    currentPage * productsPerPage,
    (currentPage + 1) * productsPerPage
  );


  const handleCreateProduct = async (event) => {
    event.preventDefault();
    const productData = {
      productName: event.target.productName.value,
      productDescription: event.target.productDescription.value,
      productCode: event.target.productCode.value,
      productStocks: event.target.productStocks.value,
      productPrice: event.target.productPrice.value
    };
    // Validate data
    if(!productData.productName || !productData.productDescription || !productData.productCode || !productData.productStocks || !productData.productPrice) {
      Swal.fire({
        title: 'FAILED!',
        icon: 'error',
        text: 'Please fill in all fields.',
      });
      return;
    }

    // convert productCode to lowercase or uppercase
    productData.productCode = productData.productCode.toLowerCase();
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
        method: "POST",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json"
        },
        body: JSON.stringify(productData)
      });
      const data = await response.json();
      if (data.message === "Product was created successfully.") {
        console.log(data.message);
        Swal.fire({
          title: 'SUCCESS!',
          icon: 'success',
          text: 'Product was created successfully.',
        });
        handleClose();
      } else if (data.message === "Product code already exists in the database.") {
        console.error(data.message);
        Swal.fire({
          title: 'FAILED!',
          icon: 'error',
          text: 'Product code already exists in the database.',
        });
      } else {
        console.error(data.error);
        // display error message to the user
      }
    } catch (error) {
      console.error(error);
    }
  };

  
  return (
    <>
      <Button variant="secondary" size="sm" onClick={handleShow}>Add a Product</Button>
       <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>ADD NEW PRODUCT</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleCreateProduct}>
            <Form.Group controlId="productName">
              <Form.Label>Name</Form.Label>
              <Form.Control type="text" placeholder="Product Name" />
            </Form.Group>
            <Form.Group controlId="productDescription">
              <Form.Label>Description</Form.Label>
              <Form.Control as="textarea" placeholder="Product Description" />
            </Form.Group>
            <Form.Group controlId="productCode">
              <Form.Label>Code</Form.Label>
              <Form.Control type="text" placeholder="Product Code" />
            </Form.Group>
            <Form.Group controlId="productStocks">
              <Form.Label>Stocks</Form.Label>
              <Form.Control type="number" placeholder="Product Stocks" />
            </Form.Group>
            <Form.Group controlId="productPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control type="number" placeholder="Product Price" />
            </Form.Group>
            <Button variant="primary" type="submit">
              Create Product
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
      <input
        type="text" id="search"
        placeholder="Search products..."
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
      />
      <Table striped bordered hover>
      <thead className="table-header">
          <tr id="table-row">
            <th>Name</th>
            <th>Description</th>
            <th>Code</th>
            <th>Stocks</th>
            <th>Price</th>
            <th>Date Created</th>
            <th>Date Updated</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {displayedProducts.sort((a, b) => new Date(b.createdOn) - new Date(a.createdOn)).map((product) => (
            <tr key={product._id}>
              <td>{product.productName}</td>
              <td>{product.productDescription}</td>
              <td>{product.productCode}</td>
              <td>{product.productStocks}</td>
              <td>{product.productPrice}</td>
              <td>{new Date(product.createdOn).toLocaleString()}</td>
              <td>{new Date(product.updatedOn).toLocaleString()}</td>
              <td>
                <Button variant="primary" onClick={() => handleViewProduct(product._id)}>View</Button>
                <Button variant="info" onClick={() => handleUpdateProduct(product._id)}>Update</Button>
                {product.isActive ? (
                  <Button onClick={() => archiveProduct(product._id)} variant="danger">Archive</Button>
                ) : (
                  <Button onClick={() => unarchiveProduct(product._id)} variant="success">Unarchive</Button>
                )}
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      <Pagination>
        <Pagination.First onClick={() => handlePageChange(0)} />
        <Pagination.Prev onClick={() => handlePageChange(currentPage - 1)} />
        {Array.from({ length: pageCount }, (_, index) => (
          <Pagination.Item
            key={index}
            active={index === currentPage}
            onClick={() => handlePageChange(index)}
          >
            {index + 1}
          </Pagination.Item>
        ))}
        <Pagination.Next onClick={() => handlePageChange(currentPage + 1)} />
        <Pagination.Last onClick={() => handlePageChange(pageCount - 1)} />
      </Pagination>
      <Modal show={viewModalShow} onHide={() => setViewModalShow(false)}>
        <Modal.Header closeButton>
          <Modal.Title>{selectedProduct.productName}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Description: {selectedProduct.productDescription}</p>
          <p>Code: {selectedProduct.productCode}</p>
          <p>Stocks: {selectedProduct.productStocks}</p>
          <p>Price: {selectedProduct.productPrice}</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setViewModalShow(false)}>Close</Button>
        </Modal.Footer>
      </Modal>
      <Modal show={updateModalShow} onHide={() => setUpdateModalShow(false)}>
      <Modal.Header closeButton>
        <Modal.Title>UPDATE PRODUCT INFORMATION</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group controlId="formProductName">
            <Form.Label>Name</Form.Label>
            <Form.Control type="text" defaultValue={selectedProduct.productName} />
          </Form.Group>
          <Form.Group controlId="formProductDescription">
            <Form.Label>Description</Form.Label>
            <Form.Control as="textarea" defaultValue={selectedProduct.productDescription} />
          </Form.Group>
          <Form.Group controlId="formProductStocks">
            <Form.Label>Stocks</Form.Label>
            <Form.Control type="number" defaultValue={selectedProduct.productStocks} />
          </Form.Group>
          <Form.Group controlId="formProductPrice">
            <Form.Label>Price</Form.Label>
            <Form.Control type="number" defaultValue={selectedProduct.productPrice} step="0.01" />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={() => setUpdateModalShow(false)}>Close</Button>
        <Button variant="primary" id="submit-changes-button" onClick={handleSaveChanges}>Save Changes</Button>
      </Modal.Footer>
    </Modal>
    </>
  );
}