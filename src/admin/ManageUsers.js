import React, { useState, useEffect } from 'react';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Swal from 'sweetalert2';


function MyVerticallyCenteredModal(props) {
  const [user, setUser] = useState({});

  useEffect(() => {
    if (props.token && props.userId) {
      fetch(`${process.env.REACT_APP_API_URL}/users/${props.userId}/viewAuser`, {
        headers: {
          Authorization: `Bearer ${props.token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setUser(data);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }, [props.token, props.userId]);

  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          USER INFORMATION
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p>
          <strong>Name:</strong> {user.firstName} {user.middleName} {user.lastName}
        </p>
        <p>
          <strong>Mobile:</strong> {user.mobileNo}
        </p>
        <p>
          <strong>Email:</strong> {user.email}
        </p>
        <p>
          <strong>Role:</strong> {user.isAdmin ? 'Admin' : 'User'}
        </p>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default function ManageUsers() {
  const token = localStorage.getItem('token');
  const [userDetails, setUserDetails] = useState({});
  const [users, setUsers] = useState([]);
  const [selectedUserId, setSelectedUserId] = useState(null);
  const [modalShow, setModalShow] = useState(false);

  useEffect(() => {
    if (token) {
      fetch(`${process.env.REACT_APP_API_URL}/users/viewAllUsers`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          // Exclude the super admin from the list of users
          const filteredUsers = data.filter((user) => user.email !== 'admin@mail.com');
          setUsers(filteredUsers);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }, [token]);

  const handleViewUser = (userId) => {
    setSelectedUserId(userId);
    setModalShow(true);
  };

  const handleSetUserToAdmin = (userId) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/setToAdmin`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({ userId }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.value === 'This user is now an admin.') {
          const updatedUsers = users.map((user) => {
            if (user._id === userId) {
              return { ...user, isAdmin: true };
            }
            return user;
          });
          setUsers(updatedUsers);
        }
        Swal.fire({
            title: 'SUCCESS!',
            icon: 'success',
            text: 'This user is now an admin.',
          });
          setTimeout(() => {
            window.location.reload();
          }, 2000);
        console.log(data.value);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleSetAdminToUser = (userId) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/setToUser`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({ userId }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.value === 'This admin is now a user.') {
          const updatedUsers = users.map((user) => {
            if (user._id === userId) {
              return { ...user, isAdmin: false };
            }
            return user;
          });
          setUsers(updatedUsers);
        }
        Swal.fire({
            title: 'SUCCESS!',
            icon: 'success',
            text: 'This admin is now a user.',
          });
          setTimeout(() => {
            window.location.reload();
          }, 2000);
        console.log(data.value);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <>
      <h1>Manage Users ({users.length})</h1>
      <Table striped  className="table-users">
        <thead>
          <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Middle Name</th>
            <th>Last Name</th>
            <th>Role</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user, index) => (
            <tr key={user._id}>
              <td>{index + 1}</td>
              <td>{user.firstName}</td>
              <td>{user.middleName}</td>
              <td>{user.lastName}</td>
              <td>{user.isAdmin ? 'Admin' : 'User'}</td>
              <td>
                <Button variant="primary" onClick={() => handleViewUser(user._id)}>View</Button>
                <Button variant="success" onClick={() => handleSetUserToAdmin(user._id)}>Set Admin</Button>
                <Button variant="info" onClick={() => handleSetAdminToUser(user._id)}>Set to User</Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      <MyVerticallyCenteredModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        userId={selectedUserId}
        token={token}
      />
    </>
  );
}