import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
 export default function Logout() {
  const { unsetUser, setUser } = useContext(UserContext);
   useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      localStorage.removeItem('token');
      unsetUser();
      setUser({ id: null });
    }
  }, [unsetUser, setUser]);
   return <Navigate to="/" />;
}