import { Form, Button }                     from 'react-bootstrap';
import { useState, useEffect, useContext }  from 'react';
import { Navigate, useNavigate }            from 'react-router-dom'; 
import UserContext                          from '../UserContext';
import Swal                                 from 'sweetalert2';


export default function Register() 
{
    const navigate = useNavigate();
    const {user} = useContext(UserContext);

    // To store and manage the values of the input fields
    const [firstName,   setFirstName]     = useState("");
    const [middleName,  setMiddleName]    = useState("");
    const [lastName,    setLastName]      = useState("");
    const [userName,    setUserName]      = useState("");
    const [mobileNo,    setMobileNo]      = useState("");
    const [email,       setEmail]         = useState("");
    const [password1,   setPassword1]     = useState("");
    const [password2,   setPassword2]     = useState("");
    // To determine wether submit button is disabled or not
    const [isActive, setIsActive]   = useState(false);

    useEffect(() => 
    {
        if ((firstName !== null && middleName !== null && lastName !== null && userName !== null && mobileNo !== null && email !== "" && password1 !== "" && password2!== "") && password1 === password2) 
        {
            setIsActive(true);
        }
        else
        {
            setIsActive(false);
        }
    }, [firstName, middleName, lastName, userName, mobileNo, email, password1, password2]);

    // Function to simulate user registration
   function registerUser(e)
    {
        e.preventDefault();

        // Check if the email already exists
        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, 
        {
			method: "POST",
			headers: 
			{
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName:  firstName,
                middleName: middleName,
                lastName:   lastName,
                userName:   userName,
                mobileNo:   mobileNo,
                email:      email,
                password:   password1,
                isAdmin:    true
			})
		})
		.then(res => res.json())
		.then(data => 
        {
			console.log(data)

			if(data === true) 
            {
            	Swal.fire({
					title: "Email already exists!",
					icon: "error",
					text: "Please enter a unique email address!"
				});

                // Clear the input fields
                setEmail("");
			}
            // Check if the mobile number is at least 11 digits
            else if (mobileNo.length < 11) 
            {
                Swal.fire({
                    title: "Invalid mobile number!",
                    icon: "error",
                    text: "Please enter a mobile number with at least 11 digits!"
                });
                return;
            }
            // Register the user
            else 
            {
                fetch(`${process.env.REACT_APP_API_URL}/users/register`, 
                {
                    method: "POST",
                    headers: 
                    {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName:  firstName,
                        middleName: middleName,
                        lastName:   lastName,
                        userName:   userName,
                        mobileNo:   mobileNo,
                        email:      email,
                        password:   password1,
                        isAdmin:    true
                    })
                })
                .then(res => res.json())
                .then(data => 
                {
                    console.log(data)

                    if(data === true) 
                    {
                        Swal.fire({
                            title: "Registered Successfully!",
                            icon: "success",
                            text: "Welcome to E-Commerce!"
                            });

                        // Clear the input fields
                        setFirstName("");
                        setMiddleName("");
                        setLastName("");
                        setUserName("");
                        setMobileNo("");
                        setEmail("");
                        setPassword1("");
                        setPassword2("");

                        navigate("/");
                    }
                    else 
                    {
                        Swal.fire({
                            title: "Something went wrong!",
                            icon: "error",
                            text: "Please try again!"
                        });
                    }
                })
			}
		})
    };

    return (
        (user.id !== null) ?
        <Navigate to="/login" />
        :
        <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group controlId="userFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                type="text" 
                placeholder="Enter First Name"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                required
                />
            </Form.Group>

            <Form.Group controlId="userMiddleName">
                <Form.Label>Middle Name</Form.Label>
                <Form.Control 
                type="text" 
                placeholder="Enter Middle Name"
                value={middleName}
                onChange={(e) => setMiddleName(e.target.value)}
                required
                />
            </Form.Group>

            <Form.Group controlId="userLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                type="text" 
                placeholder="Enter Last Name"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                required
                />
            </Form.Group>

            <Form.Group controlId="userUserName">
                <Form.Label>User Name</Form.Label>
                <Form.Control 
                type="text" 
                placeholder="Enter User Name"
                value={userName}
                onChange={(e) => setUserName(e.target.value)}
                required
                />
            </Form.Group>

            <Form.Group controlId="userMobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                type="text" 
                placeholder="Enter Mobile Number"
                value={mobileNo}
                onChange={(e) => setMobileNo(e.target.value)}
                required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                type="email" 
                placeholder="Enter Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
                />
                <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder=" Enter Password" 
                    value={password1}
                    onChange={(e) => setPassword1(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password"
                    value={password2}
                    onChange={(e) => setPassword2(e.target.value)}
                    required
                />
            </Form.Group>

            <Button variant={isActive ? "primary" : "danger"} type="submit" id="submitBtn" disabled={!isActive}>Submit</Button>
        </Form>
    )
}