import { useState, useEffect } from 'react';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import Swal from 'sweetalert2';

export default function Cart() {
  const token = localStorage.getItem('token');
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(true);
  const [totalAmount, setTotalAmount] = useState(0);

  const handleQuantityChange = (e, item) => {
    const quantity = parseInt(e.target.value);
    const subTotal = quantity * item.productPrice;

    setItems(prevItems => prevItems.map(cartItem => {
      if (cartItem._id === item._id) {
        return { ...cartItem, quantity: quantity, subTotal: subTotal };
      }
      return cartItem;
    }));
  };

  const handleUpdate = async (e, item) => {
    e.preventDefault();
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/carts/updateCart/${item._id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          quantity: item.quantity,
          subTotal: item.subTotal,
        }),
      });
      Swal.fire({
        title: 'Updated Successfully!',
        icon: 'success',
        text: 'Item quantity changed!',
      });
      const data = await response.json();
      console.log("Response:", data); // log the response to the console
      const updatedItems = data.items.filter((cartItem) => cartItem.quantity > 0);
      setItems(updatedItems);
      setTotalAmount(data.totalAmount);
      localStorage.setItem("cartItems", JSON.stringify(updatedItems));
      console.log("Updated items:", updatedItems); // log the updated items to the console
      console.log("Total amount:", data.totalAmount); // log the total amount to the console
    } catch (error) {
      console.log("An error occurred while updating the cart: ", error);
    }
  };


  const handleRemove = async (e, item) => {
    e.preventDefault();
    console.log("Item ID:", item._id); // log the item ID to the console
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/carts/removeCartItem/${item._id}`, {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      });
      const data = await response.json();
      console.log("Response:", data); // log the response to the console
      setItems(items => items.filter(cartItem => cartItem._id !== item._id));
      setTotalAmount(data.totalAmount);
      localStorage.setItem("cartItems", JSON.stringify(items.filter(cartItem => cartItem._id !== item._id)));
      console.log("Updated items:", items.filter(cartItem => cartItem._id !== item._id)); // log the updated items to the console
      console.log("Total amount:", data.totalAmount); // log the total amount to the console
      Swal.fire({
        title: 'Removed Successfully!',
        icon: 'success',
        text: 'Item removed from cart!',
      });
    } catch (error) {
      console.log("An error occurred while removing the item from the cart: ", error);
    }
  };


  useEffect(() => {
    async function fetchData() {
      try {
        const response = await fetch(
          `${process.env.REACT_APP_API_URL}/carts/viewCart`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );
        const data = await response.json();
        console.log(data);
        if (data.items) {
          setItems(data.items);
        } else {
          setItems([]);
        }
        setLoading(false);
      } catch (error) {
        console.log(error);
      }
    }
    if (token) {
      fetchData();
    }
  }, [token]);

  useEffect(() => {
    const amount = items.reduce((acc, item) => {
      return acc + parseFloat(item.subTotal);
    }, 0);
    setTotalAmount(amount);
  }, [items]);
  
  return (
    <>
      <h1>{`CART [${items.length}]`}</h1>
      {loading ? (
        <p>Loading...</p>
      ) : (
        <>
          {items.length === 0 ? (
            <p>No items found in your cart.</p>
          ) : (
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Product Name</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Subtotal</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {items.map(item => (
                  <tr key={item._id}>
                    <td>{item.productName}</td>
                    <td>&#8369;{item.productPrice}</td>
                    <td>
                      <input
                        type="number"
                        min = {1}
                        value={item.quantity}
                        onChange={(e) => handleQuantityChange(e, item)}
                      />
                    </td>
                    <td>&#8369;{item.subTotal}</td>
                    <td>
                      <Button variant="primary" onClick={e => handleUpdate(e, item)}>Update</Button>
                      <Button variant="danger" onClick={(e) => handleRemove(e, item)}>Remove</Button>
                    </td>
                  </tr>
                ))}
              </tbody>
              <tfoot>
                <tr>
                  <td colSpan="2"></td>
                  <td><strong>TOTAL AMOUNT</strong></td>
                  <td><strong>&#8369;{totalAmount}</strong></td>
                  <td><Button variant="success">CHECKOUT ITEMS</Button></td>
                </tr>
              </tfoot>
            </Table>
          )}
        </>
      )}
    </>
  );
}