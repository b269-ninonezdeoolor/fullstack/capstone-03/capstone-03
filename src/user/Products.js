import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Card, Button, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Modal from 'react-bootstrap/Modal';
import Pagination from 'react-bootstrap/Pagination';
import Swal from 'sweetalert2';


export function ViewProduct(props) {
  const { selectedProductId, setSelectedProduct, show, onHide } = props;
  const [product, setProduct] = useState(null);
  const [quantity, setQuantity] = useState(1);
  
   useEffect(() => {
    if (selectedProductId) {
      fetch(`${process.env.REACT_APP_API_URL}/products/${selectedProductId}/viewAproduct`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      })
        .then((response) => response.json())
        .then((data) => {
          if (data && data.product) {
            setProduct(data.product);
          } else {
            console.error('Invalid data returned from API');
          }
        })
        .catch((error) => console.error(error));
    }
  }, [selectedProductId]);
   function handleIncrement() {
    setQuantity(prevQuantity => prevQuantity + 1);
  }
   function handleDecrement() {
    setQuantity(prevQuantity => prevQuantity === 1 ? 1 : prevQuantity - 1);
  }

  function handleClick() {
    fetch(`${process.env.REACT_APP_API_URL}/carts/addToCart`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        productId: selectedProductId,
        quantity: quantity,
      }),
    })
      .then(() => {
        // Hide the modal after adding to cart
        onHide();
        Swal.fire({
          title: 'Added Successfully!',
          icon: 'success',
          text: 'Item added to cart.',
        });
      })
      .catch((error) => {
        console.error(error);
      });
  }
   return (
    <Modal show={show} onHide={onHide}>
      <Modal.Header closeButton>
        <Modal.Title>Product Details</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {product && (
          <>
            <p>Name: {product.productName}</p>
            <p>Description: {product.productDescription}</p>
            <p>Code: {product.productCode}</p>
            <p>Stocks: {product.productStocks}</p>
            <p>Price: &#8369;{product.productPrice}</p>
            <p>Quantity: </p>
            <div className="quantity-input">
              <button className="decrement-button" onClick={handleDecrement}>-</button>
              <input type="number" value={quantity} onChange={event => setQuantity(Number(event.target.value))} />
              <button className="increment-button" onClick={handleIncrement}>+</button>
            </div>
          </>
        )}
      </Modal.Body>
      <Modal.Footer className="modal-footer">
        <div className="buttons-left">
          <Button variant="primary" onClick={handleClick}>
            Add to Cart
          </Button>
        </div>
        <Button variant="secondary" onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

 export default function Products() {
  const token = localStorage.getItem('token');
  const [userDetails, setUserDetails] = useState({});
  const [products, setProducts] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');
  const [modalShow, setModalShow] = useState(false);
  const [selectedProductId, setSelectedProductId] = useState(null);
  const [currentPage, setCurrentPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState(16);
   useEffect(() => {
    if (token) {
      // Fetch user details
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setUserDetails(data); // Set user details state
        })
        .catch((error) => {
          console.log(error);
        });
      // Fetch products
      fetch(`${process.env.REACT_APP_API_URL}/products/viewAllActiveProducts`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => res.json())
        .then((data) => {
          setProducts(data); // Set products state
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }, [token]);

  const activeProducts = products.filter(
    (product) =>
      product.isActive === true &&
      (product.productName.toLowerCase().includes(searchQuery.toLowerCase()) ||
        product.productDescription.toLowerCase().includes(searchQuery.toLowerCase()))
  ).slice((currentPage - 1) * itemsPerPage, currentPage * itemsPerPage);

   const totalPages = Math.ceil(products.length / itemsPerPage);
   const handleSearch = (event) => {
    setSearchQuery(event.target.value);
    setCurrentPage(1);
  };

   const handleViewProduct = (productId) => {
    setSelectedProductId(productId);
    setModalShow(true);
  };

   const handleHideModal = () => {
    setSelectedProductId(null);
    setModalShow(false);
  };

   const handlePageChange = (page) => {
    setCurrentPage(page);
  };

   return (
    <Container>
      <h1>Welcome, {userDetails && userDetails.firstName}!</h1>
      <Form.Group>
        <Form.Control type="text" placeholder="Search products..." value={searchQuery} onChange={handleSearch} />
      </Form.Group>
      <Row>
        {activeProducts.map((product) => (
          <Col xs={12} sm={6} md={4} lg={3} key={product._id}>
            <Card>
              <Card.Img variant="top" src={`https://via.placeholder.com/75x50.png?text=Sample+Image`} />
              <Card.Body>
                <Card.Title>{product.productName}</Card.Title>
                <Card.Text>{product.productDescription}</Card.Text>
                <Button variant="primary" onClick={() => handleViewProduct(product._id)}>
                  View Product
                </Button>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
      <Pagination>
        <Pagination.First onClick={() => handlePageChange(1)} />
        <Pagination.Prev onClick={() => handlePageChange(currentPage - 1)} disabled={currentPage === 1} />
        {[...Array(totalPages)].map((_, i) => (
          <Pagination.Item key={i} active={i + 1 === currentPage} onClick={() => handlePageChange(i + 1)}>
            {i + 1}
          </Pagination.Item>
        ))}
        <Pagination.Next onClick={() => handlePageChange(currentPage + 1)} disabled={currentPage === totalPages} />
        <Pagination.Last onClick={() => handlePageChange(totalPages)} />
      </Pagination>
      <ViewProduct selectedProductId={selectedProductId} setSelectedProduct={setSelectedProductId} show={modalShow} onHide={handleHideModal} />
    </Container>
  );
}